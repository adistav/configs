source ~/fugitive/plugin/fugitive.vim
source ~/fugitive/autoload/fugitive.vim

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set textwidth=119
set autoindent

set grepprg=git\ grep\ -n\ \"$@\"

set number
set ruler
set incsearch
set hlsearch
set listchars=tab:␉·,trail:␠,nbsp:⎵,extends:→,precedes:←
set list
set wildmode=longest,list
set path=.,,**
set clipboard=unnamed
set mouse=a
syntax on

map gO :!open -a 'Google Chrome' %
map gG :grep -Fw <cfile>
